/*
    Author : Smit Pattel / Lefevre Florian
    Date : 05/12/20 - ?/?/?
    Description : Try to move some IA from the bed
*/

// Include

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_image.h>
#include <string.h>

// Structure

struct Enemies{
    SDL_Rect enemy;
    int state;
};

typedef struct{
    char name[5];
    int number;
}Score;



// Prototype

// Create the "body" of the enemies :

void createEnemies(struct Enemies *position, SDL_Renderer * renderer);

// Calculate random spawning position of enemies :

int randomPosition();
int specRandomPosition(struct Enemies *position);
int attributePosition(int * bufferPos);

// To do pause maybe it'll be replace

void pause(SDL_Event event);

// Menu prototype :

void mainMenu();
void mainGame(int * colorSkin);
void scoreMenu();
void skinsMenu();

// Dependencies :

bool checkCollision(SDL_Rect A, SDL_Rect B);
void skinsModel(SDL_Renderer * renderer, int * color , int posX);

void printAllScores();
int biggtosmaller();


// Function

/*---------------------------------------------------------------------------*/
/*          Section contain every dependencies for the the main game         */
/*---------------------------------------------------------------------------*/

/*
    Function : Calculate and return a valid random position
*/
int randomPosition(){
    int nPos = 0;
    int rangePos[6] = {50,150,250,350,450,550};
    int temp = rand() % 6;
    nPos = rangePos[temp];
    return nPos;
}

/*
    Function initialize a buffer & create enemy 3 by 3
*/

void createEnemies(struct Enemies *car, SDL_Renderer * renderer){
    int setBuffer[3] = {0,0,0};
    int i = 0;
    for(i = 0; i < 3; i++){
        if(i == 0){
            car[i].enemy.x = randomPosition();
            setBuffer[i] = car[i].enemy.x;
        }else{
            car[i].enemy.x = attributePosition(setBuffer);
            setBuffer[i] = car[i].enemy.x;
        }
        car[i].enemy.y = 20;
        SDL_SetRenderDrawColor(renderer, 0, 0, 255, 255);
        SDL_RenderFillRect(renderer, &car[i].enemy);
        SDL_RenderPresent(renderer);
    }

}

/*
    Function : Calculate a valid position before returning this (check if this valid position is not taken by another entity)
*/

int attributePosition(int * bufferPos){
    int setBuffer[3];
    int countSetUp = 0;
    int randNumber = 0;
    for(int i = 0; i < 3; i++){                                 // Remember to add state checkup
        setBuffer[i] = bufferPos[i];
    }
    while(countSetUp != 1){
        randNumber = randomPosition();
        if(randNumber != setBuffer[0] && randNumber != setBuffer[1] && randNumber != setBuffer[2]){
            countSetUp = 1;
        }
    }
    return randNumber;
}

/*
    Function : Calculate collision between two entities (not working yet)
*/

bool checkCollision(SDL_Rect A, SDL_Rect B){

    // Rectangle side to compare

    int leftA;
    int leftB;
    int rightA;
    int rightB;
    int topA;
    int topB;
    int bottomA;
    int bottomB;

    // Calculate every side of A

    leftA = A.x + A.h;
    rightA = leftA + A.w;
    topA = A.x + A.w;
    bottomA = topA + A.h;

    // Calculate every side of B

    leftB = B.x + B.h;
    rightB = leftB + B.w;
    topB = B.x + B.w;
    bottomB = topB + B.h;

    // Compare & collide detection

    if(bottomA <= topB){
        return false;
    }

    if(topA >= bottomB){
        return false;
    }

    if(rightA <= leftB){
        return false;
    }

    if(leftA >= rightB){
        return false;
    }

    // If step compare & collide is full false so there's a condition and so collide is true

    return true;

}

void printAllScores(){
    Score score;
    Score tabScore[10];
    int n = 0;
    FILE *fp = fopen("score.txt","rb");
    printf("\nAffichage des scores : ");
    while(fread(&score,sizeof(Score),1,fp), !feof(fp)){
        tabScore[n] = score;
        n++;
    }
    int temp = 0;
    int tempName[6];
    for(int i = 0; i < 10; i++){
        for(int j = 0; j < 10 - 1; j++){
            if(tabScore[j].number < tabScore[j+1].number){
                temp = tabScore[j].number;
                strcpy(tempName,tabScore[j].name);
                tabScore[j].number = tabScore[j+1].number;
                strcpy(tabScore[j].name,tabScore[j+1].name);
                tabScore[j+1].number = temp;
                strcpy(tabScore[j+1].name,tempName);
            }
        }
    }
    printf("\nBest score : ");
    for(int m = 0; m < 5; m++){
        printf("\n%d",tabScore[m].number);
        printf(" scored by : %s",tabScore[m].name);
    }

    fclose(fp);
}

int biggtosmaller(){
    Score score;
    Score tabScore[10];
    int n = 0;
    FILE *fp = fopen("score.txt","rb");
    while(fread(&score,sizeof(Score),1,fp), !feof(fp)){
        tabScore[n] = score;
        n++;
    }
    int temp = 0;
    for(int i = 0; i < 10; i++){
        for(int j = 0; j < 10 - 1; j++){
            if(tabScore[j].number < tabScore[j+1].number){
                temp = tabScore[j].number;
                tabScore[j].number = tabScore[j+1].number;
                tabScore[j+1].number = temp;
            }
        }
    }
    fclose(fp);
    return tabScore[4].number;
}
/*---------------------------------------------------------------------------*/
/*                      This is our main for the game                        */
/*---------------------------------------------------------------------------*/

/*
    Function execute the main game
*/

void mainGame(int * colorSkin){

    SDL_Event eventMove;

    SDL_Rect player;
    SDL_Rect scoreRect;

    SDL_Window* window = NULL;
    SDL_Window * scoreWindow = NULL;

    SDL_Color scoreColor = {0,255,0};

    SDL_Surface* texte = NULL;

    int collisionState;

    //unsigned long scorePlayer;
    unsigned long * seconde;

    char printer[50];

    SDL_Renderer* renderer = NULL;
    SDL_Renderer* rendererScore = NULL;

    char * font_path;

    clock_t start = clock();

    window = SDL_CreateWindow
    (
        "Car-crash beta 1.5", SDL_WINDOWPOS_UNDEFINED,
        SDL_WINDOWPOS_UNDEFINED,
        650,
        1025,
        SDL_WINDOW_SHOWN
    );



    scoreWindow = SDL_CreateWindow
    (
        "Score", 515,
        50,
        100,
        100,
        SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE
    );

    SDL_Surface * psurf = SDL_GetWindowSurface(scoreWindow);

    renderer =  SDL_CreateRenderer( window, -1, SDL_RENDERER_ACCELERATED);      // Initialise le renderer
                                                                                // -> Scene pour le moteur de rendu
                                                                                // ->
                                                                                // -> Fa�on dont ce d�clare le moteur de rendu

    rendererScore = SDL_CreateRenderer(scoreWindow,-1,SDL_RENDERER_ACCELERATED);

    SDL_SetRenderDrawColor( renderer, 255, 255, 255, 0 );                       // D�finie la couleur du BG � blanc

    SDL_RenderClear( renderer );                                                // Supprime l'�cran pr�c�dent "reset"

    // INIT TTF :

    TTF_Init();
    font_path = "ariali.ttf";
    TTF_Font *font = TTF_OpenFont(font_path,24);
    if(font == NULL){
        fprintf(stderr, "error : font not found \n");
        exit(EXIT_FAILURE);
    }

    // Create the player body :

    player.x = 350;
    player.y = 850;
    player.w = 50;
    player.h = 100;

    SDL_SetRenderDrawColor(renderer,colorSkin[0],colorSkin[1],colorSkin[2],0);
    SDL_RenderFillRect(renderer,&player);
    SDL_RenderPresent(renderer);

    // Create enemies bodies :

    srand(time(NULL));

    struct Enemies * enemies_array = malloc(sizeof(struct Enemies) * 3);
    for(int i = 0; i < 3; i++){
        enemies_array[i].enemy.x = randomPosition();
        enemies_array[i].enemy.y = 20;
        enemies_array[i].enemy.w = 50;
        enemies_array[i].enemy.h = 100;
        enemies_array[i].state = 1;
    }

    // init spawning interval for enemies

    int i = 1;
    int timeBeforeInvoke = 0;
    int timeNowInvoke = 0;

    while(i)
    {

        for(int j = 0; j < 3; j++){
           enemies_array[j].enemy.y++;
        }

        while(SDL_PollEvent(&eventMove) !=0) //While playing (with player alive)
        {
            if(eventMove.type == SDL_QUIT){
                i=0;
            }
            else if(eventMove.type == SDL_KEYDOWN) // When key from keyboard is pressed
            {
                switch(eventMove.key.keysym.sym)
                {
                case SDLK_ESCAPE:
                case SDLK_q:
                    i=0;
                break;
                case SDLK_UP:
                    player.y -=50;
                    if(player.y < 150){
                        player.y = 150;
                    }
                break;
                case SDLK_DOWN:
                    player.y +=50;
                    if(player.y > 850){
                        player.y = 850;
                    }
                break;
                case SDLK_RIGHT:
                    player.x +=100;
                    if(player.x > 550){
                        player.x = 550;
                    }
                break;
                case SDLK_LEFT:
                    player.x -=100;
                    if(player.x < 0){
                        player.x = 50;
                    }
                break;
                case SDLK_h:
                    i = 0;
                break;
                }
            }
        }

        // Rendering & animation

        SDL_SetRenderDrawColor(renderer,255,255,255,0); //Remettre tout le fond en bleu
        SDL_RenderClear(renderer);

        // Animate the player (re print player body) :

        SDL_SetRenderDrawColor(renderer,colorSkin[0],colorSkin[1],colorSkin[2],255);
        SDL_RenderFillRect(renderer,&player);

        // Animate the enemies (re print enemies bodies) :

        SDL_SetRenderDrawColor(renderer,0,0,0,255);
        for(int j = 0; j < 3; j++){
            SDL_RenderFillRect(renderer,&enemies_array[j].enemy);
        }

        timeNowInvoke = SDL_GetTicks();

        if(timeNowInvoke - timeBeforeInvoke >= 3000){
            createEnemies(enemies_array,renderer);
            timeBeforeInvoke = timeNowInvoke;
        }

        SDL_RenderPresent(renderer);

        // Score :

        SDL_FillRect(psurf,NULL,SDL_MapRGB(psurf->format,0,0,0));
        scoreRect.x = 0;
        scoreRect.y = 0;
        SDL_BlitSurface(texte,NULL,psurf,&scoreRect);

        clock_t mid = clock();
        seconde = (mid - start) / CLOCKS_PER_SEC;
        sprintf(printer,"%ld",seconde);
        texte = TTF_RenderText_Blended(font,printer,scoreColor);

        SDL_UpdateWindowSurface(scoreWindow);

    }

    // Total score calculating...

    clock_t end = clock();
    seconde = (end - start) / CLOCKS_PER_SEC;
    printf("Votre score : %ld\n", seconde);

    printf("\nTapez votre nom : ");
    char name[5];
    scanf("%s",&name);

    FILE * fp = fopen("score.txt","r+b");
    if(fp == NULL ){
        fp = fopen("score.txt","w+b");
    }

    int tabScore = biggtosmaller();

    if(seconde > tabScore){
        fwrite(seconde,sizeof(unsigned long), 1,fp);
    }
    fclose(fp);

    // End the program

    TTF_CloseFont(font);
    TTF_Quit();

    SDL_DestroyRenderer(renderer);
    SDL_DestroyRenderer(rendererScore);
    SDL_DestroyWindow(window);
    SDL_DestroyWindow(scoreWindow);

    free(enemies_array);
    mainMenu();

}

/*---------------------------------------------------------------------------*/
/*                         This is our menu section                          */
/*---------------------------------------------------------------------------*/

/*
    Function : create main menu
*/

void mainMenu(){
    SDL_Window* window = NULL;

    window = SDL_CreateWindow
    (
        "Car-crash beta 1.2",
        SDL_WINDOWPOS_UNDEFINED,
        SDL_WINDOWPOS_UNDEFINED,
        200,
        300,
        SDL_WINDOW_SHOWN
    );

    SDL_Renderer* renderer = NULL;
    renderer =  SDL_CreateRenderer( window, -1, SDL_RENDERER_ACCELERATED);
    SDL_SetRenderDrawColor( renderer, 255, 255, 255, 0 );

    SDL_RenderClear( renderer );

    SDL_Event eventMainMenu;
    SDL_Rect startButton;
    SDL_Rect scoreButton;

    startButton.x = 50;
    startButton.y = 50;
    startButton.w = 100;
    startButton.h = 50;

    scoreButton.x = 50;
    scoreButton.y = 150;
    scoreButton.w = 100;
    scoreButton.h = 50;

    SDL_SetRenderDrawColor(renderer, 0, 255, 0, 255);
    SDL_RenderFillRect(renderer, &startButton);
    SDL_RenderPresent(renderer);

    SDL_SetRenderDrawColor(renderer, 0, 255, 155, 255);
    SDL_RenderFillRect(renderer, &scoreButton);
    SDL_RenderPresent(renderer);

    // Temporary remember to delete this :

    printf("\nMenu principal !");

    int pause = 1;
    while(pause == 1){
        SDL_WaitEvent(&eventMainMenu);

        SDL_RenderPresent(renderer);

        switch(eventMainMenu.type){
            case SDL_QUIT:
                pause = 0;
                break;
            case SDL_MOUSEBUTTONDOWN:
                if(eventMainMenu.button.button == SDL_BUTTON_LEFT){
                    if(eventMainMenu.button.x > startButton.x
                       && eventMainMenu.button.x < startButton.x + startButton.w
                       && eventMainMenu.button.y > startButton.y
                       && eventMainMenu.button.y < startButton.y + startButton.h){
                            SDL_DestroyRenderer(renderer);
                            SDL_DestroyWindow(window);
                            skinsMenu();
                       }
                    if(eventMainMenu.button.x > scoreButton.x
                       && eventMainMenu.button.x < scoreButton.x + scoreButton.w
                       && eventMainMenu.button.y > scoreButton.y
                       && eventMainMenu.button.y < scoreButton.y + scoreButton.h){
                            SDL_DestroyRenderer(renderer);
                            SDL_DestroyWindow(window);
                            scoreMenu();
                       }
                }
                break;
        }

    }

    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
}

/*
    Function : Create the score menu (See every score contain in score.dat)

*/

void scoreMenu(){
    SDL_Window* window = NULL;

    SDL_Renderer* renderer = NULL;

    SDL_Rect goBackButton;

    SDL_Event eventScoreMenu;

    int pause = 1;

    window = SDL_CreateWindow
    (
        "Car-crash beta 1.2", SDL_WINDOWPOS_UNDEFINED,
        SDL_WINDOWPOS_UNDEFINED,
        200,
        500,
        SDL_WINDOW_SHOWN
    );

    renderer =  SDL_CreateRenderer( window, -1, SDL_RENDERER_ACCELERATED);
    SDL_SetRenderDrawColor( renderer, 255, 255, 255, 0 );

    SDL_RenderClear( renderer );

    goBackButton.x = 10;
    goBackButton.y = 440;
    goBackButton.w = 100;
    goBackButton.h = 50;

    /*
    SDL_Window* scoreWin = NULL;
    scoreWin = SDL_CreateWindow
    (
        "Car-crash beta 1.2", SDL_WINDOWPOS_UNDEFINED,
        SDL_WINDOWPOS_UNDEFINED,
        200,
        500,
        SDL_WINDOW_SHOWN
    );

    SDL_Surface *psurf = SDL_GetWindowSurface(scoreWin);

    TTF_Init();
    TTF_Font *font = TTF_OpenFont("ariali.ttf",24);
    if(font == NULL){
        fprintf(stderr,"error : font not found \n");
        exit(EXIT_FAILURE);
    }

    SDL_Surface * texte = NULL;
    char * content= "bonjour";

    SDL_Color black = {255,0,0};

    SDL_Rect position;
    position.x = 10;
    position.y = 10;
    SDL_FillRect(psurf,NULL,SDL_MapRGB(psurf->format,255,255,255));
    SDL_BlitSurface(texte,NULL,psurf,&position);

    texte = TTF_RenderText_Blended(font,"Bonjour",black);

    SDL_UpdateWindowSurface(scoreWin);
    */
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
    SDL_RenderFillRect(renderer, &goBackButton);
    SDL_RenderPresent(renderer);

    printAllScores();

    // Temporary remember to delete this :

    printf("\nMenu des scores  !");

    while(pause == 1){
        SDL_WaitEvent(&eventScoreMenu);
        switch(eventScoreMenu.type){
            case SDL_QUIT:
                pause = 0;
                break;
            case SDL_MOUSEBUTTONDOWN:
                if(eventScoreMenu.button.button == SDL_BUTTON_LEFT){
                    if(eventScoreMenu.button.x > goBackButton.x
                       && eventScoreMenu.button.x < goBackButton.x + goBackButton.w
                       && eventScoreMenu.button.y > goBackButton.y
                       && eventScoreMenu.button.y < goBackButton.y + goBackButton.h){
                            SDL_DestroyRenderer(renderer);
                            SDL_DestroyWindow(window);
                            mainMenu();
                       }
                }
        }
    }
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
}

/*
    Function : Add a skins menu , where the player can choose his skin before launching the main game
*/

void skinsMenu(){
    SDL_Window* window = NULL;
    window = SDL_CreateWindow
    (
        "Car-crash beta 1.2", SDL_WINDOWPOS_UNDEFINED,
        SDL_WINDOWPOS_UNDEFINED,
        600,
        500,
        SDL_WINDOW_SHOWN
    );

    SDL_Renderer* renderer = NULL;
    renderer =  SDL_CreateRenderer( window, -1, SDL_RENDERER_ACCELERATED);
    SDL_SetRenderDrawColor( renderer, 255, 255, 255, 0 );

    SDL_RenderClear( renderer );

    SDL_Event eventSkinMenu;
    SDL_Rect goBackButton;
    SDL_Rect skin1Select;
    SDL_Rect skin2Select;
    SDL_Rect skin3Select;

    goBackButton.x = 10;
    goBackButton.y = 440;
    goBackButton.w = 100;
    goBackButton.h = 50;

    skin1Select.x = 150;
    skin1Select.y = 250;
    skin1Select.w = 50;
    skin1Select.h = 50;

    skin2Select.x = 250;
    skin2Select.y = 250;
    skin2Select.w = 50;
    skin2Select.h = 50;

    skin3Select.x = 350;
    skin3Select.y = 250;
    skin3Select.w = 50;
    skin3Select.h = 50;

    // Draw skin section :

    int tab[3] = {0,99,140};
    skinsModel(renderer, tab, 150);

    int tab2[3] = {238,130,238};
    skinsModel(renderer, tab2, 250);

    int tab3[3] = {102,255,102};
    skinsModel(renderer, tab3, 350);

    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
    SDL_RenderFillRect(renderer, &goBackButton);
    SDL_RenderPresent(renderer);

    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
    SDL_RenderFillRect(renderer, &skin1Select);
    SDL_RenderPresent(renderer);

    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
    SDL_RenderFillRect(renderer, &skin2Select);
    SDL_RenderPresent(renderer);

    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
    SDL_RenderFillRect(renderer, &skin3Select);
    SDL_RenderPresent(renderer);

    // Temporary remember to delete this :

    printf("\nSelection des skins !");

    int pause = 1;
    while(pause == 1){
        SDL_WaitEvent(&eventSkinMenu);
        switch(eventSkinMenu.type){
            case SDL_QUIT:
                pause = 0;
                break;
            case SDL_MOUSEBUTTONDOWN:
                if(eventSkinMenu.button.button == SDL_BUTTON_LEFT){
                    if(eventSkinMenu.button.x > goBackButton.x
                       && eventSkinMenu.button.x < goBackButton.x + goBackButton.w
                       && eventSkinMenu.button.y > goBackButton.y
                       && eventSkinMenu.button.y < goBackButton.y + goBackButton.h){
                            SDL_DestroyRenderer(renderer);
                            SDL_DestroyWindow(window);
                            mainMenu();

                       }
                    if(eventSkinMenu.button.x > skin1Select.x
                       && eventSkinMenu.button.x < skin1Select.x + skin1Select.w
                       && eventSkinMenu.button.y > skin1Select.y
                       && eventSkinMenu.button.y < skin1Select.y + skin1Select.h){
                            SDL_DestroyRenderer(renderer);
                            SDL_DestroyWindow(window);
                            mainGame(tab);
                       }
                    if(eventSkinMenu.button.x > skin2Select.x
                       && eventSkinMenu.button.x < skin2Select.x + skin2Select.w
                       && eventSkinMenu.button.y > skin2Select.y
                       && eventSkinMenu.button.y < skin2Select.y + skin2Select.h){
                            SDL_DestroyRenderer(renderer);
                            SDL_DestroyWindow(window);
                            mainGame(tab2);
                       }
                    if(eventSkinMenu.button.x > skin3Select.x
                       && eventSkinMenu.button.x < skin3Select.x + skin3Select.w
                       && eventSkinMenu.button.y > skin3Select.y
                       && eventSkinMenu.button.y < skin3Select.y + skin3Select.h){
                            SDL_DestroyRenderer(renderer);
                            SDL_DestroyWindow(window);
                            mainGame(tab3);

                       }
                }
        }
    }
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
}

/*
    Function dependency of skin menu add example for player skin (available in game later...)
*/

void skinsModel(SDL_Renderer * renderer, int * color , int posX){

    SDL_Rect skinModel;
    skinModel.x = posX;
    skinModel.y = 50;
    skinModel.w = 50;
    skinModel.h = 100;
    SDL_SetRenderDrawColor(renderer, color[0], color[1], color[2], 255);
    SDL_RenderFillRect(renderer, &skinModel);
    SDL_RenderPresent(renderer);

    printf("Done skin !");

}

/*---------------------------------------------------------------------------*/
/*                             Run the main code...                          */
/*---------------------------------------------------------------------------*/

int main (int argc, char** argv)
{
    mainMenu();
    return EXIT_SUCCESS;
}


